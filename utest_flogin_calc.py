import unittest
import flogin_calc
import time
from systemd import journal
import locale


class FloginCalcTest(unittest.TestCase):

    def test_getLastFailedLogins(self):
        locale.setlocale(locale.LC_ALL, "ru_RU.UTF-8")        
        distance =  time.time() - time.mktime( time.strptime("2019/04/30", "%Y/%m/%d" ) )

        self.assertEqual( flogin_calc.getLastFailedLogins( lastSec = distance, path = "test_data/"  ), 3)
        locale.resetlocale(locale.LC_ALL)

if __name__ == '__main__':
    unittest.main()