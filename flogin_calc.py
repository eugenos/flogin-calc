#!/usr/bin/env python3.6
# coding: utf-8
def getLastFailedLogins( lastSec = 1 * 3600, path = None, fd = None ):
    import time
    from systemd import journal

    '''
        lastSec - time in seconds for that to search (default 1 hour)
        path    - file, directory or filesystem path to use (default None)
        fd      - file desc to use to (default None)
    '''

    if path is not None:
        j = journal.Reader( path = path )
    elif fd is not None:
        j = journal.Reader( path = fd )
    else:
        j = journal.Reader()

    j.add_match("SYSLOG_FACILITY=10", "PRIORITY=5", "SYSLOG_IDENTIFIER=login", "SYSLOG_IDENTIFIER=sshd")
    distance = time.time() - lastSec
    j.seek_realtime(distance)

    counter = 0                       
    for x in j:
        
        if "authentication failure" in x["MESSAGE"]:
            counter += 1
            # print(f"x:{x}\n")
            # print(f"================\n")
    
    j.close()
    return counter

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Get failed login count')
    parser.add_argument('--path', type=str, nargs='?', help='path to systemd journal', default = "")
    parser.add_argument('--time', type=int, nargs='?', help='check last N hours', default = 0)

    args = parser.parse_args()    

    lastSec = args.time * 3600
    if lastSec == 0:
        lastSec = 1 * 3600
    
    path = args.path

    if path == "":
        print( getLastFailedLogins( lastSec ) )
    else:
        print( getLastFailedLogins( lastSec, path = path ) )

