**flogin_calc.py**
    
Get failed login count module
Can be run from console or used as module.

Console mode  

`flogin_calc.py [--time=hours] [--path=path_to_journal]`  

 `--path` - path to systemd journal, default - using system journal  
 `--time` - check last N hours, default - 1 hour
 
As module  

def getLastFailedLogins( lastSec = 1 * 3600, path = None, fd = None )  

returns failed login count.  

lastSec - time in seconds for what to search in journal
path - path to directory or filesystem with journal
fd -   journal as a descriptor 

by default used system journal and last 1 hour  
path or fd can be used only separately (if used both, only path argument 
is working)
    
 
    
**utest_flogin_calc.py**
    
`utest_flogin_calc.py` - unittest
`test_data` - test journal snap, for unittest
